var express = require("express");
var path = require("path");
var bodyParser = require('body-parser');
var app = express();
var axios = require('axios');
var PORT = process.env.PORT || 8000;
var search = [];
var firebase = require('firebase');
var parseString = require('xml2js').parseString;
var _ = require('lodash');


// firebase config

var config = {
  apiKey: "AIzaSyDnQYeH8dvtDNC8tIZMecc6Y0uaDLTqH-I",
  authDomain: "foodsearch-82582.firebaseapp.com",
  databaseURL: "https://foodsearch-82582.firebaseio.com",
  projectId: "foodsearch-82582",
  storageBucket: "foodsearch-82582.appspot.com",
  messagingSenderId: "378088961315"
};

firebase.initializeApp(config);

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse application/json
app.use(bodyParser.json());
// firebase pull
var ref = firebase.app().database().ref();
ref.once('value')
 .then(function (snap) {
 console.log(snap.val(), snap.val());
 snap.push(search);
 });

session();
// request for session id and url
function session(){{
  var loginRequestMXL =
`<?xml version="1.0" encoding="utf-8" ?>

<env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema"

    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"

    xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">

  <env:Body>

    <n1:login xmlns:n1="urn:partner.soap.sforce.com">

      <n1:username>david.p.hoffmann1@gmail.com</n1:username>

      <n1:password>Password123!!ezxKeF6AmLVfX1Az53vn78JAG</n1:password>

    </n1:login>

  </env:Body>

</env:Envelope>
`

  axios.request({

      
          method : 'POST',
          url : 'https://login.salesforce.com/services/Soap/u/43.0',
          data : loginRequestMXL,
          headers : { 
              'content-type' : 'text/xml; charset=UTF-8',
              "SOAPAction": "login" 
          }
      
  })
  .then(function(response){
      // console.log(response);
      // xml to json
      parseString(response.data, function (err, result) {
      // console.log(result);
      var object = result;
      var sessionId = _.get(object, "soapenv:Envelope.soapenv:Body.loginResponse.result", "sessionId");
      console.log(sessionId);
      var sessionUrl = _.get(object, "soapenv:Envelope.soapenv:Body", "sessionUrl");
      console.log(sessionUrl);
      
      // upsert
      axios({
      method: 'POST',
      Url : sessionUrl,
      headers: {
      sessionsId : sessionId
    },

    data: {
    RecentSearches: search,

    }
    });
      });
          })
  .catch(function(error){
      console.log(error);
  });
      }
        }


// Serve static content for the app from the "public" directory in the application directory.
app.use(express.static("public"));
// index file path
app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "index.html"));
});

// start server
app.listen(PORT, function() {
  console.log("App listening on PORT " + PORT);
});